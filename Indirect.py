import random
import tensorflow as tf





    
in_str_len = 3
population = 10
generations = 10


        
def ga():
    agents = init_agents(population, in_str_len)
    fit = []
    for _ in range(population):
        fit.append(float(-1))
        random.choice(agents)

    for generation in range(generations):

        print('Generation: ' + str(generation))
        

        print(fit)
        fit = fitness(agents, fit)
        agents = selection(agents, fit)
        agents = crossover(agents)
        agents = mutation(agents)

        #if any(agent.fitness >= 90 for agent in agents):

         #   print( 'Threshold met')
          #  exit(0)


def init_agents(population, length):
    agents = []
    for _ in range(population):
        agent = random.sample(range(1, 101), length)
        agents.append(agent)
    
    return agents

def fitness(agents, fit):
    from tensorflow.examples.tutorials.mnist import input_data

    mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)
        

    for agentz in agents:
   

        n_classes = 10
        batch_size = 100

        x = tf.placeholder('float',[None, 784])
        y = tf.placeholder('float')

        hidden_layers = []
        layers = []

        def neural_network_model(data):
            for index in range(len(agentz)):
                if index < 1:
                    temporal = {'weights':tf.Variable(tf.random_normal([784, agentz[index]])), 'biases':tf.Variable(tf.random_normal([agentz[index]]))}
                    hidden_layers.append(temporal)
                    tempy1 = tf.add(tf.matmul(data,(hidden_layers[index])['weights']),( hidden_layers[index])['biases'])
                    tempy2 = tf.nn.relu(tempy1)
                    layers.append(tempy2)
                else:
                    temporal = {'weights':tf.Variable(tf.random_normal([agentz[index-1], agentz[index]])), 'biases':tf.Variable(tf.random_normal([agentz[index]]))}
                    hidden_layers.append(temporal)
                    print (layers[index-1])
                    print (hidden_layers[index])
                    tempy1 = tf.add(tf.matmul(layers[index-1], (hidden_layers[index])['weights']),( hidden_layers[index])['biases'])
                    tempy2 = tf.nn.relu(tempy1)
                    layers.append(tempy2)

                                      
            output_layer = {'weights':tf.Variable(tf.random_normal([agentz[len(agentz)-1], n_classes])), 'biases':tf.Variable(tf.random_normal([n_classes]))}

            output = tf.matmul(layers[len(layers)-1], output_layer['weights']) + output_layer['biases']

            return output

        def train_neural_network(x):
            prediction = neural_network_model(x)
            cost = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(logits=prediction,labels=y) )
            optimizer = tf.train.AdamOptimizer().minimize(cost)
            hm_epochs = 10
            

            with tf.Session() as sess:
                sess.run(tf.global_variables_initializer())

                for epoch in range(hm_epochs):
                    epoch_loss = 0
                    for _ in range(int(mnist.train.num_examples/batch_size)):
                        epoch_x, epoch_y = mnist.train.next_batch(batch_size)
                        _, c = sess.run([optimizer,cost], feed_dict = {x: epoch_x, y: epoch_y})
                        epoch_loss += c
                    print('epoch', epoch, 'completed out of', hm_epochs,'loss:', epoch_loss)
   
                correct = tf.equal(tf.argmax(prediction,1), tf.argmax(y,1))
                accuracy = tf.reduce_mean(tf.cast(correct,'float'))
                print('Accuracy:',accuracy.eval({x:mnist.test.images, y:mnist.test.labels}))
                index = agents.index(agentz)
                accy =  accuracy.eval({x:mnist.test.images, y:mnist.test.labels})
                
                fit[index] = accy



        train_neural_network(x)
        
    return fit

def selection(agents, fit):
    agents = [x for (y,x) in sorted(zip(fit, agents))]
    agents = agents[::-1]
    print('before   ', agents)
    agents = agents[:int(0.4 * len(agents))]
    print('babee     ', agents)
    return agents
def crossover(agents):
    offspring = []

    print('oh yeaa  ' , ((population - len(agents))/2))
   
    for _ in range(int((population- len(agents))/2)):

        import random as lib_random
        print (random.__file__)
        parent1 = random.choice(agents)
        parent2 = random.choice(agents)

        split = random.randint(0, in_str_len)
        child1 = parent1[0:split] + parent2[split:in_str_len]
        child2 = parent2[0:split] + parent1[split:in_str_len]

        offspring.append(child1)
        offspring.append(child2)

    agents.extend(offspring)
    print('mybbbbaa     ' , agents)
    return agents

def mutation(agents):
    for agent in agents:
        for idx, param in enumerate(agent):
            if random.uniform(0.0,1.0) <= 0.1:
                print(agent)
                agent = agent[0:idx] + [random.randint(1, 101)] + agent[idx+1:in_str_len]
    return agents
        
        


if __name__ == '__main__':
    ga()
