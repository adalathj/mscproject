import random
import tensorflow as tf
import numpy
from timeit import default_timer as timer

# mergex = list(epoch_x[i])
  #                      mergey = list(epoch_y[i])
   #                     for k in range(9):
     #                       mergey = mergey + list(epoch_y[i])
    #                        mergex = mergex + list(epoch_x[i])

    
in_str_len = 3
population = 10
generations = 100


        
def ga():
    agents = init_agents(population, in_str_len)
    fit = []
    for _ in range(population):
        fit.append(float(0.0))
        random.choice(agents)
    best = 0
    for generation in range(generations):

        print('Generation: ' + str(generation))
        

        print(fit)
        fit = fitness(agents, fit, population)
        if(best<max(fit)):
            best = max(fit)
        print('fitness is   ', best)
        agents = selection(agents, fit)
        agents = selection(agents, fit)
        agents = crossover(agents)
       # agents = mutation(agents)

        #if any(agent.fitness >= 90 for agent in agents):

         #   print( 'Threshold met')
          #  exit(0)


def init_agents(population, length):
    agents = []
    for _ in range(population):
        agent = random.sample(range(400, 450), length)
        agents.append(agent)
    
    return agents

def fitness(agents, fit, population):
    from tensorflow.examples.tutorials.mnist import input_data

    mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)
   
    size = []
    boundaries = []
    n_classes = 10
    resultboundaries = []
    x = tf.placeholder('float',[None, 784])
    y = tf.placeholder('float')
   
    
    
    for hidden in range(3):
        sizeh = 0
        boundariesh = []

        for agentz in agents:
            if(hidden ==0):
                resultboundaries.append(10*(agents.index(agentz)+1))


            sizeh = sizeh + agentz[hidden]
            boundariesh.append(sizeh)
        size.append(sizeh)
        boundaries.append(boundariesh)

        
    batch_size = 100
  
    hidden_layers = []
    layers = []


    def get_constants(inputsize, outputsize, inputboundaries, outputboundaries):
        constants = []
        for i in range(inputsize):
            constants.append([])
            for z in range(len(inputboundaries)):
                if i >= inputboundaries[z]:
                    x = z+1
                elif i < inputboundaries[0]:
                    x = 0
            for j in range(outputsize):

                for t in range(len(outputboundaries)):
                    if j >= outputboundaries[t]:
                        new = t+1
                        y = new
                    elif j < outputboundaries[0]:
                        y = 0
                if x==y:
                    constants[i].append(1.0)
                else:
                    constants[i].append(0.0)
        return constants

    def neural_network_model(datas):
        for index in range(3):
            print('Initialising Parameters')
            if index < 1:
                temporal = {'weights':tf.Variable(tf.random_normal([784, size[index]]))}
                hidden_layers.append(temporal)
                tempy1 = tf.matmul(datas,  (hidden_layers[index])['weights'])
                tempy2 = tf.nn.relu(tempy1)
                layers.append(tempy2)
            else:
                
                constant = get_constants(size[index-1], size[index], boundaries[index-1], boundaries[index])

                temporal = {'weights':tf.Variable(tf.random_normal([size[index-1], size[index]]))}
                hidden_layers.append(temporal)
                result = []

                c = tf.constant(constant)

                finalweights = numpy.multiply(c, (hidden_layers[index])['weights'])
                tempy1 = tf.matmul(layers[index-1], finalweights)
                tempy2 = tf.nn.relu(tempy1)
                layers.append(tempy2)


        constant = get_constants(size[2], len(agents)*n_classes, boundaries[2], resultboundaries)

        c = tf.constant(constant)
                      
        output_layer = {'weights':tf.Variable(tf.random_normal([size[len(size)-1], 100]))}
        finalweights = numpy.multiply(c, (output_layer)['weights'])
        output = tf.matmul(layers[len(layers)-1], finalweights)

        return output

    def train_neural_network(x):
        start = timer()

        prediction = neural_network_model(x)
        cost = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(logits=prediction,labels=y) )
        optimizer = tf.train.AdamOptimizer().minimize(cost)
        hm_epochs = 10
        
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
        sess = tf.Session(config=config)
        with tf.Graph().as_default(),sess:

            
            sess.run(tf.global_variables_initializer())
            for epoch in range(hm_epochs):
                epoch_loss = 0
                print('Training')


                for _ in range(int(mnist.train.num_examples/batch_size)):
                    epoch_x, epoch_y = mnist.train.next_batch(batch_size)
                    nepoy = []
                    for i in range(batch_size):
                        newoney = []
                        
                        for d in range(len(epoch_y[i])):
                            newoney.append((epoch_y[i])[d])
                        mergey = newoney

                        for k in range(9):
                            mergey = mergey+newoney

                        nepoy.append(mergey)

                    _, c = sess.run([optimizer,cost], feed_dict = {x: epoch_x, y: nepoy})
                    epoch_loss += c

                print('epoch', epoch, 'completed out of', hm_epochs,'loss:', epoch_loss)

            end = timer()    
            list1 = [population, end-start]
            
            with open('time.txt', 'w') as file:
                file.write(str(list1))
     
            
            predictionz=prediction
            testSet = mnist.test.next_batch(1000)
            thelist =  predictionz.eval(feed_dict={x: testSet[0]}, session=sess)
            print('this is', thelist)
            labels = mnist.test.labels
        
        fitness = []
        for k in range(len(thelist)):
                
            for i in range(len(agents)):
                last = i*10
                myindex = 9+i*10
                indiprediction = thelist[k][last:myindex]
                    
                print(indiprediction)
                z, = numpy.where( indiprediction==max(indiprediction) )
                j, = numpy.where( labels[k] == max(labels[k]))
                if(len(z) > 1):
                    z  = [j[0]+1]

                
                if(j==z):

                    fit[i] += 0.009
        print(fit)
        
        tf.reset_default_graph()
        sess.close()
        del sess
    train_neural_network(x)
        
        
        
    return fit

def selection(agents, fit):
    agents = [x for (y,x) in sorted(zip(fit, agents))]
    agents = agents[::-1]

    agents = agents[:int(0.5 * len(agents))]

    return agents
def crossover(agents):
    offspring = []


   
    for _ in range(int((population- len(agents))/2)):

        import random as lib_random
        print (random.__file__)
        parent1 = random.choice(agents)
        parent2 = random.choice(agents)

        split = random.randint(0, in_str_len)
        child1 = parent1[0:split] + parent2[split:in_str_len]
        child2 = parent2[0:split] + parent1[split:in_str_len]

        offspring.append(child1)
        offspring.append(child2)

    agents.extend(offspring)

    return agents

def mutation(agents):
    for agent in agents:
        for idx, param in enumerate(agent):
            if random.uniform(0.0,1.0) <= 0.1:
                print(agent)
                agent = agent[0:idx] + [random.randint(1, 101)] + agent[idx+1:in_str_len]
    return agents
        
        


if __name__ == '__main__':
    ga()
