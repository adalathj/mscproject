import random
import tensorflow as tf
import numpy
from timeit import default_timer as timer



    
in_str_len = 3
population = 14
generations = 10
hidden_nodes = 784


        
def ga():
    resultlist = []
    agents = init_agents(population, in_str_len)
    fit = []
    for _ in range(population):
        fit.append(float(-1))
        random.choice(agents)

    for generation in range(generations):
        start = timer()
        print('Generation: ' + str(generation))
        

        print(fit)
        fit = fitness(agents, fit, generation, resultlist)
        agents = selection(agents, fit)
        end = timer()
        print('time is   ' ,end-start)
       # agents = crossover(agents)
        agents = mutation(agents)

        #if any(agent.fitness >= 90 for agent in agents):

         #   print( 'Threshold met')
          #  exit(0)


def init_agents(population, length):
    agents = []
    for _ in range(population):
        agent  = numpy.random.randint(2, size=( 784*3+10, 784))
        agents.append(agent)
         
    return agents

def fitness(agents, fit, generation, resultlist):
        
        
    from tensorflow.examples.tutorials.mnist import input_data

    mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)
    bestindex = 0
    fitnez = fit
        
    number = -1
    for agentz in agents:
        number += 1

        print(number, '  Numba')

        n_classes = 10
        batch_size = 100

        x = tf.placeholder('float',[None, 784])
        y = tf.placeholder('float')

        hidden_layers = []
        layers = []
        def neural_network_model(data):
            for index in range(3):
                print(index)
                if index < 1:
                    temporal = {'weights':tf.Variable(tf.random_normal([784, 784])), 'biases':tf.Variable(tf.random_normal([784]))}
                    hidden_layers.append(temporal)
                    agen = tf.constant( numpy.asfarray(agentz[:784], dtype='float32'))
                    finalweights = numpy.multiply(agen, (hidden_layers[index])['weights'])
                    tempy1 = tf.add(tf.matmul(data, finalweights),( hidden_layers[index])['biases'])
                    tempy2 = tf.nn.relu(tempy1)
                    layers.append(tempy2)
                else:
                    temporal = {'weights':tf.Variable(tf.random_normal([784, 784])), 'biases':tf.Variable(tf.random_normal([784]))}
                    hidden_layers.append(temporal)
                    agen = tf.constant( numpy.asfarray(agentz[784*index:784*(index+1)], dtype='float32'))
                    finalweights = numpy.multiply(agen, (hidden_layers[index])['weights'])
                    
                    tempy1 = tf.add(tf.matmul(layers[index-1], finalweights),( hidden_layers[index])['biases'])
                    tempy2 = tf.nn.relu(tempy1)
                    layers.append(tempy2)

           
            output_layer = {'weights':tf.Variable(tf.random_normal([784, n_classes])), 'biases':tf.Variable(tf.random_normal([n_classes]))}

            agen = tf.constant((numpy.asfarray(agentz[2352:2362])).transpose(), dtype='float32')
            finalweights = numpy.multiply(agen, output_layer['weights'])
            output = tf.matmul(layers[len(layers)-1],finalweights) + output_layer['biases']

            return output

        def train_neural_network(x):
            prediction = neural_network_model(x)
            cost = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(logits=prediction,labels=y) )
            optimizer = tf.train.AdamOptimizer().minimize(cost)
            hm_epochs = 10
            

            with tf.Session() as sess:
                sess.run(tf.global_variables_initializer())

                for epoch in range(hm_epochs):
                    epoch_loss = 0
                    for _ in range(int(mnist.train.num_examples/batch_size)):
                        epoch_x, epoch_y = mnist.train.next_batch(batch_size)
                        _, c = sess.run([optimizer,cost], feed_dict = {x: epoch_x, y: epoch_y})
                        epoch_loss += c
                    print('epoch', epoch, 'completed out of', hm_epochs,'loss:', epoch_loss)
                    if generation > 0:
                        bestindex = fit.index(max(fitnez))
                    if generation > 0 and bestindex == number:
                         correct = tf.equal(tf.argmax(prediction,1), tf.argmax(y,1))
                         accuracy = tf.reduce_mean(tf.cast(correct,'float'))
                         print('Accuracy:',accuracy.eval({x:mnist.test.images, y:mnist.test.labels}))
                         accy =  accuracy.eval({x:mnist.test.images, y:mnist.test.labels})
                         list = [accy, generation, epoch+1]
                         resultlist.append(list)
                         with open("file.txt", "w") as output:
                             output.write(str(resultlist))
                correct = tf.equal(tf.argmax(prediction,1), tf.argmax(y,1))
                accuracy = tf.reduce_mean(tf.cast(correct,'float'))
                print('Accuracy:',accuracy.eval({x:mnist.test.images, y:mnist.test.labels}))
                accy =  accuracy.eval({x:mnist.test.images, y:mnist.test.labels})
                fit[number] = accy
            sess.close()
            del sess
        train_neural_network(x)
        
    return fit

def selection(agents, fit):
    i = 0
    lowest = fit[0]
    low = agents[0]
    newfit = []
    newagent = []
    while len(fit) > 0:
        if  fit[i] < lowest:
            lowest = fit[i]
            low = agents[i]
        i += 1
        if i == len(fit):
            newagent.append(low)
            newfit.append(lowest)
            fit.remove(lowest)

            if fit:
                lowest = fit[0]
                low = agents[0]
            i = 0

    print(newfit)
    fit = newfit
    newagent = newagent[::-1]

    agents = newagent[:int(0.5 * len(agents))]
    return agents
def crossover(agents):
    offspring = []

    print('oh yeaa  ' , ((population - len(agents))/2))
   
    for _ in range(int((population- len(agents))/2)):

        import random as lib_random
        print (random.__file__)
        parent1 = random.choice(agents)
        parent2 = random.choice(agents)

        split = random.randint(0, in_str_len)
        child1 = parent1[0:split] + parent2[split:in_str_len]
        child2 = parent2[0:split] + parent1[split:in_str_len]

        offspring.append(child1)
        offspring.append(child2)

    agents.extend(offspring)
    print('mybbbbaa     ' , agents)
    return agents

def mutation(agents):
    for _ in range(int(population- len(agents))):
        import random as lib_random
        print (random.__file__)
        child = random.choice(agents)
        for i in range(len(agents[1])):
            for j in range(len(agents[1][1])):
                if random.uniform(0.0, 1.0) <=2/ len(agents[1])*len(agents[1][1]):
                    if child[i][j] ==1:
                        child[i][j] = 0
                    else:
                        child[i][j] = 1
        agents.append(child)
    return agents
        
        


if __name__ == '__main__':
    ga()
